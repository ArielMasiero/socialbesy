package com.besysoft.core.SocialBesy.controller;

import com.besysoft.core.SocialBesy.domain.Desc;
import com.besysoft.core.SocialBesy.domain.TipoDato;
import com.besysoft.core.SocialBesy.dto.UserDto;
import com.besysoft.core.SocialBesy.dto.UserOnlyFollowDto;
import com.besysoft.core.SocialBesy.dto.UserOnlyFollowerDto;
import com.besysoft.core.SocialBesy.exceptions.CustomException;
import com.besysoft.core.SocialBesy.service.interfaces.UserInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/usuarios")
public class UserController {

    Logger log = LoggerFactory.getLogger(UserController.class);
    private UserInterface userService;

    @Autowired
    public UserController(UserInterface userService) {
        this.userService = userService;
    }

    @GetMapping("/{userId}")
    public ResponseEntity<Map<String,Object>> getUserbyId(@PathVariable("userId") Long userId) {
        Map<String, Object> response = new HashMap<>();
        Optional<UserDto> result = userService.getUser(userId);
        if (result.isPresent()){
            response.put("result",result.get());
        }else{
            response.put("result","no existe usuario");
        }
        return ResponseEntity.ok(response);
    }
    @PostMapping("")
    public ResponseEntity<Map<String,Object>> saveUser(@RequestParam(name = "nombre_Usuario")String nombreUsuario){
        log.info("saveUser nombre_Usuario: {}",nombreUsuario);
        Map<String,Object> response = new HashMap<>();
        try {
            Optional<UserDto> result= userService.saveUser(nombreUsuario);
            if (result.isPresent()){
                response.put("sucess",true);
                response.put("message","se actualizaron las listas de los usuarios");
                return ResponseEntity.ok(response);
            }else{
                response.put("sucess",false);
                response.put("message","no funciona");
                return ResponseEntity.badRequest().body(response);
            }
        }catch(Exception e){
            response.put("sucess",false);
            response.put("error",e.getMessage());
            return ResponseEntity.badRequest().body(response);
        }
    }

    @GetMapping("")
    public ResponseEntity<Map<String,Object>> getAllUser(){
        Map<String,Object> response = new HashMap<>();
        List<UserDto> result = userService.getAllUser();
        try {
            if (result != null && !result.isEmpty()) {
                response.put("result", result);
            } else {
                response.put("result", "no existe usuario");
            }
            return ResponseEntity.ok(response);
        }catch (Exception e){
            response.put("result","no se pudo realizar la operacion");
            response.put("error",e.getMessage());
            return ResponseEntity.badRequest().body(response);
        }
    }

    @PostMapping("/{userId}/seguir/{userIDASeguir}")
    public ResponseEntity<Map<String,Object>> seguir(@PathVariable("userId") Long userId,@PathVariable("userIDASeguir") Long userIdASeguir){
        log.info("seguir userid: {},userIDASeguir: {}",userId,userIdASeguir);
        Map<String,Object> response = new HashMap<>();
        try {
            userService.seguir(userId,userIdASeguir);
            response.put("sucess",true);
            response.put("message","se actualizaron las listas de los usuarios");
            return ResponseEntity.ok(response);
        }catch(CustomException.ClienteNoExiste | CustomException.Autoseguirse e){
            response.put("sucess",false);
            response.put("error",e.getMessage());
            return ResponseEntity.badRequest().body(response);
        }
    }

    @GetMapping("/{userId}/seguidores/listado")
    public ResponseEntity<Map<String,Object>> getDTOUserOnlyFollowerbyId(@PathVariable("userId") Long userId){
        Map<String,Object> response = new HashMap<>();
        try{
            Optional<UserOnlyFollowerDto> result = userService.getUserOnlyFollower(userId);
            if (result.isPresent()){
                response.put("result",result.get());
            }else{
                response.put("result","no existe usuario");
            }
            return ResponseEntity.ok(response);
        }catch (Exception e){
            response.put("result","no se pudo realizar la operacion");
            response.put("error",e.getMessage());
            return ResponseEntity.badRequest().body(response);
        }
    }

    @GetMapping("/{userId}/sigo/listado")
    public ResponseEntity<Map<String,Object>> getDTOUserOnlyFollowbyId(@PathVariable("userId") Long userId){
        Map<String,Object> response = new HashMap<>();
        try {
            Optional<UserOnlyFollowDto> result = userService.getUserOnlyFollow(userId);
            if (result.isPresent()) {
                response.put("result", result.get());
            } else {
                response.put("result", "no existe usuario");
            }
            return ResponseEntity.ok(response);
        }catch (Exception e){
            response.put("result","no se pudo realizar la operacion");
            response.put("error",e.getMessage());
            return ResponseEntity.badRequest().body(response);
        }
    }

    @GetMapping("/{userId}/seguidores/listado/ordenado")
    public ResponseEntity<Map<String,Object>> getUserSeguidoresOrdenadoById(@PathVariable("userId") Long userId, @RequestParam(name = "odenar")TipoDato tipoDato,@RequestParam(name = "desc")Desc desc){
        Map<String,Object> response = new HashMap<>();
        try{
            Optional<UserOnlyFollowerDto> result = userService.getUserFollowerByidOrdernado(userId,tipoDato,desc);
            if(result.isPresent()){
                response.put("result",result);
            }else{
                response.put("result","No se pudo realizar la operacion");
            }
            return ResponseEntity.ok(response);
        }catch (Exception e){
        response.put("result","no se pudo realizar la operacion");
        response.put("error",e.getMessage());
        return ResponseEntity.badRequest().body(response);
    }
    }

    @GetMapping("/{userId}/sigo/listado/ordenado")
    public ResponseEntity<Map<String,Object>> getUserSeguidosOrdenadoById(@PathVariable("userId") Long userId, @RequestParam(name = "odenar")TipoDato tipoDato,@RequestParam(name = "desc") Desc desc){
        Map<String,Object> response = new HashMap<>();
        try {
            Optional<UserOnlyFollowDto> result = userService.getUserFollowByidOrdernado(userId, tipoDato, desc);
            if (result.isPresent()) {
                response.put("result", result);
            } else {
                response.put("result", "No se pudo realizar la operacion");
            }
            return ResponseEntity.ok(response);
        }catch (Exception e){
            response.put("result","no se pudo realizar la operacion");
            response.put("error",e.getMessage());
            return ResponseEntity.badRequest().body(response);
        }
    }
}
