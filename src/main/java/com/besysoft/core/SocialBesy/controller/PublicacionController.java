package com.besysoft.core.SocialBesy.controller;

import com.besysoft.core.SocialBesy.domain.Publicacion;
import com.besysoft.core.SocialBesy.dto.PublicacionDto;
import com.besysoft.core.SocialBesy.dto.PublicacionRequest;
import com.besysoft.core.SocialBesy.dto.UserPublicationDto;
import com.besysoft.core.SocialBesy.service.interfaces.PublicacionInterface;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/productos")
public class PublicacionController {

    Logger log = LoggerFactory.getLogger(PublicacionController.class);
    private PublicacionInterface service;

    @Autowired
    public PublicacionController(PublicacionInterface service) {
        this.service = service;
    }

    @GetMapping("")
    public ResponseEntity<Map<String,Object>> getallPublications(){
        Map<String,Object> response = new HashMap<>();
        try {
            Iterable<PublicacionDto> result = service.getAll();
            if(!result.equals(null)){
                response.put("result", result);
                return ResponseEntity.ok(response);
            }else{
                response.put("result", "no se recuperaron datos de publicacion");
                return ResponseEntity.badRequest().body(response);
            }

        }catch (Exception e){
            response.put("result","no se pudo realizar la operacion");
            response.put("error",e.getMessage());
            return ResponseEntity.badRequest().body(response);
        }
    }

    @GetMapping("/{userId}/listado")
    public ResponseEntity<Map<String,Object>> getUserPublicationsbyId(@PathVariable("userId") Long userId){
        Map<String,Object> response = new HashMap<>();
        try {
            Optional<UserPublicationDto> result = service.getUserPublication(userId);
            if (result.isPresent()) {
                response.put("result", result.get());
                return ResponseEntity.ok(response);
            } else {
                response.put("result", "Usuario no existe");
                return ResponseEntity.badRequest().body(response);
            }
        }catch (Exception e){
            response.put("result","no se pudo realizar la operacion");
            response.put("error",e.getMessage());
            return ResponseEntity.badRequest().body(response);
        }
    }

    @PostMapping("/publicacion")
    public ResponseEntity<Map<String,Object>> savePublication(@RequestBody PublicacionRequest publicacionRequest){
        Map<String,Object> response = new HashMap<>();
        try {
            Optional<Long> result = service.publicar(publicacionRequest);
            if (result.isPresent()) {
                response.put("result", result.get());
                return ResponseEntity.ok(response);
            } else {
                response.put("result", "Usuario no existe");
                return ResponseEntity.badRequest().body(response);
            }
        }catch (Exception e){
            response.put("result","no se pudo realizar la operacion");
            response.put("error",e.getMessage());
            return ResponseEntity.badRequest().body(response);
        }
    }
}
