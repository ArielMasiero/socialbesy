package com.besysoft.core.SocialBesy.service.interfaces;

import com.besysoft.core.SocialBesy.dto.PublicacionDto;
import com.besysoft.core.SocialBesy.dto.PublicacionRequest;
import com.besysoft.core.SocialBesy.dto.UserPublicationDto;

import java.util.Optional;

public interface PublicacionInterface {
    public Iterable<PublicacionDto> getAll();
    public Optional<Long> publicar(PublicacionRequest publicarRequest);
    public Optional<UserPublicationDto> getUserPublication(Long id);
}
