package com.besysoft.core.SocialBesy.service.implementation;

import com.besysoft.core.SocialBesy.domain.Desc;
import com.besysoft.core.SocialBesy.domain.TipoDato;
import com.besysoft.core.SocialBesy.domain.User;
import com.besysoft.core.SocialBesy.domain.UserNotFollower;
import com.besysoft.core.SocialBesy.dto.UserDto;
import com.besysoft.core.SocialBesy.dto.UserNotFollowerDto;
import com.besysoft.core.SocialBesy.dto.UserOnlyFollowDto;
import com.besysoft.core.SocialBesy.dto.UserOnlyFollowerDto;
import com.besysoft.core.SocialBesy.exceptions.CustomException;
import com.besysoft.core.SocialBesy.mapper.UserMapper;
import com.besysoft.core.SocialBesy.mapper.UserNotFollowerMapper;
import com.besysoft.core.SocialBesy.repository.RepositoryInterface;
import com.besysoft.core.SocialBesy.service.interfaces.UserInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
@Service
public class UserImpl implements UserInterface {

    private RepositoryInterface service;
    private UserMapper userMapper;
    private UserNotFollowerMapper userNotFollowerMapper;
    Logger log = LoggerFactory.getLogger(UserImpl.class);

    @Autowired
    public UserImpl(RepositoryInterface service, UserMapper userMapper,UserNotFollowerMapper userNotFollowerMapper) {
        this.service = service;
        this.userMapper = userMapper;
        this.userNotFollowerMapper = userNotFollowerMapper;
    }

    @Override
    public Optional<UserDto> getUser(Long id) {
        log.info("getUser (id: {}",id);
        Optional<UserDto> userDto;
        Optional<User> user =service.getUserByid(id);
        if(!user.isEmpty()){
            log.info("user: {}",user.toString());
            userDto=Optional.of(userMapper.getUserDto(user.get()));
        }else{
            userDto=Optional.empty();
        }
        return userDto;
    }

    @Override
    public Optional<UserDto> saveUser(String nombre_usuario) {
        try {
            UserDto user = userMapper.getUserDto(service.saveUser(new User(null, nombre_usuario)));
            return Optional.of(user);
        }catch (Exception e){
            log.error("fallo al agregar usuario");
            log.error(e.getMessage());
            return Optional.empty();
        }
    }

    @Override
    public List<UserDto> getAllUser() {
        return userMapper.getAllUserDto(service.getAllUsers());
    }

    @Override
    public void seguir(Long idUsuario, Long idUsuarioFollow) throws CustomException.ClienteNoExiste, CustomException.Autoseguirse {
        log.info("Seguir (idUsuario: {},idUsuarioFollow :{}",idUsuario,idUsuarioFollow);
        Optional<User> user = service.getUserByid(idUsuario);
        Optional<User> userASeguir =service.getUserByid(idUsuarioFollow);
        if(!user.isEmpty() && !userASeguir.isEmpty()){
            log.info("Seguir (user: {}",user.get());
            log.info("Seguir (userASeguir: {}",userASeguir.get());

            //logica de seguir usuario
            UserNotFollower userNotFollower = userNotFollowerMapper.getUserNotFollowerToUser(userASeguir.get());
            userNotFollower.setDate(new Date());
            log.info("userNotFollower (user: {}",userNotFollower);
            List<UserNotFollower> addFollow=this.getList(user.get().getSeguidos());
            if(!this.contieneUsuario(addFollow,idUsuarioFollow)&&!idUsuarioFollow.equals(idUsuario)) {
                addFollow.add(userNotFollower);
            }else{
                if(idUsuarioFollow.equals(idUsuario)){
                    throw new CustomException.Autoseguirse(idUsuario);
                }else{
                    log.info("Usuario ya sigue a: {}",idUsuarioFollow);
                }
            }
            log.info("addFollow: {}",addFollow);

            user.get().setSeguidos(addFollow);

            //logica de añadir a seguidores
            UserNotFollower userNotFollower1 = userNotFollowerMapper.getUserNotFollowerToUser(user.get());
            userNotFollower1.setDate(new Date());
            log.info("userNotFollower1 (user: {}",userNotFollower1);
            List<UserNotFollower> addFollowers= this.getList(userASeguir.get().getSeguidores());
            if(!this.contieneUsuario(addFollowers,idUsuario)&&!idUsuario.equals(idUsuarioFollow)){
                addFollowers.add(userNotFollower1);
            }else{
                if(idUsuario.equals(idUsuarioFollow)){
                    throw new CustomException.Autoseguirse(idUsuarioFollow);
                }else{
                    log.info("seguidor ya registrado por usuario: {}",idUsuarioFollow);
                }
            }

            userASeguir.get().setSeguidores(addFollowers);

            service.saveUser(user.get());
            service.saveUser(userASeguir.get());
        }else {
            if(user.isEmpty()) throw new CustomException.ClienteNoExiste(idUsuario);
            if(userASeguir.isEmpty()) throw new CustomException.ClienteNoExiste(idUsuarioFollow);
        }
    }

    public List<UserNotFollower> getList(List<UserNotFollower> list){
        if(list==null||list.size()==0){
            return new ArrayList<>();
        }else{
            return new ArrayList<>(list);
        }
    }

    public Boolean contieneUsuario(List<UserNotFollower> users,Long id){
        if(users.stream().anyMatch(user -> user.getUserId().equals(id))){
            return true;
        }else {
            return false;
        }

    }
    @Override
    public Optional<UserOnlyFollowDto> getUserOnlyFollow(Long id) {
        log.info("getUserOnlyFollow( id: {})",id);
        Optional<User> user = service.getUserByid(id);
        if(user.isPresent()){
            return Optional.of(userMapper.getUserOnlyFollowDto(user.get()));
        }else{
            return Optional.empty();
        }
    }

    @Override
    public Optional<UserOnlyFollowerDto> getUserOnlyFollower(Long id) {
        log.info("getUserOnlyFollower( id: {})",id);
        Optional<User> user = service.getUserByid(id);
        if(user.isPresent()){
            return Optional.of(userMapper.getUserOnlyFollowerDto(user.get()));
        }else{
            return Optional.empty();
        }
    }

    @Override
    public Optional<UserOnlyFollowerDto> getUserFollowerByidOrdernado(Long id, TipoDato tipoDato, Desc desc) {
        log.info("getUserFollowerByidOrdernado");
        Optional<User> userFollowerByidOrdernado = service.getUserFollowerByidOrdernado(id, tipoDato, desc);
        if(userFollowerByidOrdernado.isPresent()) {
            return Optional.of(userMapper.getUserOnlyFollowerDto(userFollowerByidOrdernado.get()));
        }else{
            return Optional.empty();
        }
    }

    @Override
    public Optional<UserOnlyFollowDto> getUserFollowByidOrdernado(Long id, TipoDato tipoDato, Desc desc) {
        Optional<User> userFollowByidOrdernado = service.getUserFollowByidOrdernado(id, tipoDato, desc);
        if(userFollowByidOrdernado.isPresent()) {
            return Optional.of(userMapper.getUserOnlyFollowDto(userFollowByidOrdernado.get()));
        }else{
            return Optional.empty();
        }
    }


}
