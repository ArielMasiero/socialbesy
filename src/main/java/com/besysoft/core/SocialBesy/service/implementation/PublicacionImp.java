package com.besysoft.core.SocialBesy.service.implementation;

import com.besysoft.core.SocialBesy.domain.Producto;
import com.besysoft.core.SocialBesy.domain.Publicacion;
import com.besysoft.core.SocialBesy.domain.User;
import com.besysoft.core.SocialBesy.domain.UserNotFollower;
import com.besysoft.core.SocialBesy.dto.PublicacionDto;
import com.besysoft.core.SocialBesy.dto.PublicacionRequest;
import com.besysoft.core.SocialBesy.dto.UserPublicationDto;
import com.besysoft.core.SocialBesy.mapper.PublicacionMapper;
import com.besysoft.core.SocialBesy.mapper.UserMapper;
import com.besysoft.core.SocialBesy.mapper.UserNotFollowerMapper;
import com.besysoft.core.SocialBesy.repository.RepositoryInterface;
import com.besysoft.core.SocialBesy.service.interfaces.PublicacionInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
@Service
public class PublicacionImp implements PublicacionInterface {

    private RepositoryInterface service;
    private UserMapper userMapper;
    private PublicacionMapper publicacionMapper;
    Logger log = LoggerFactory.getLogger(UserImpl.class);

    @Autowired
    public PublicacionImp(RepositoryInterface service, UserMapper userMapper,PublicacionMapper publicacionMapper) {
        this.service = service;
        this.userMapper = userMapper;
        this.publicacionMapper=publicacionMapper;
    }

    @Override
    public Iterable<PublicacionDto> getAll() {
        return publicacionMapper.getAllPublicacionDto(service.getAllPublicaciones());
    }

    @Override
    public Optional<Long> publicar(PublicacionRequest publicarRequest) {
        Optional<User> user = service.getUserByid(publicarRequest.getUserId());
        if(user.isPresent()) {
            Publicacion publicacion = service.savePublicacion(this.publicacionMapper(publicarRequest));
            List<Publicacion> publicaciones = this.getList(user.get().getPublicaciones());
            publicaciones.add(publicacion);
            user.get().setPublicaciones(publicaciones);
            service.saveUser(user.get());
            return Optional.of(publicacion.getPublicacion_id());
        }else{
            return Optional.empty();
        }
    }

    @Override
    public Optional<UserPublicationDto> getUserPublication(Long id) {
        Optional<User> user = service.getUserByid(id);
        if(user.isPresent()){
            return Optional.of(userMapper.getUserPublicationDto(user.get()));
        }else{
            return Optional.empty();
        }

    }

    private List<Publicacion> getList(List<Publicacion> list){
        if(list==null||list.size()==0){
            return new ArrayList<>();
        }else{
            return new ArrayList<>(list);
        }
    }

    private Publicacion publicacionMapper(PublicacionRequest publicacionRequest){
        return new Publicacion(null,
                new Date(),
                new Producto(
                        publicacionRequest.getDetalle().getProduct_ID(),
                        publicacionRequest.getDetalle().getProducto_nombre(),
                        publicacionRequest.getDetalle().getTipo(),
                        publicacionRequest.getDetalle().getMarca(),
                        publicacionRequest.getDetalle().getColor(),
                        publicacionRequest.getDetalle().getObservaciones()),
                publicacionRequest.getPrecio(),
                publicacionRequest.getCategoria());
    }

}
