package com.besysoft.core.SocialBesy.service.interfaces;

import com.besysoft.core.SocialBesy.domain.Desc;
import com.besysoft.core.SocialBesy.domain.TipoDato;
import com.besysoft.core.SocialBesy.domain.User;
import com.besysoft.core.SocialBesy.dto.UserDto;
import com.besysoft.core.SocialBesy.dto.UserOnlyFollowDto;
import com.besysoft.core.SocialBesy.dto.UserOnlyFollowerDto;
import com.besysoft.core.SocialBesy.exceptions.CustomException;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

public interface UserInterface {
    public Optional<UserDto> getUser(Long id);
    public Optional<UserDto> saveUser(String nombre_usuario);
    public List<UserDto> getAllUser();
    public void seguir(Long idUsuario,Long idUsuarioFollow) throws CustomException.ClienteNoExiste, CustomException.Autoseguirse;
    public Optional<UserOnlyFollowDto> getUserOnlyFollow(Long id);
    public Optional<UserOnlyFollowerDto> getUserOnlyFollower(Long id);
    Optional<UserOnlyFollowerDto> getUserFollowerByidOrdernado(Long id, TipoDato tipoDato, Desc desc);
    Optional<UserOnlyFollowDto> getUserFollowByidOrdernado(Long id, TipoDato tipoDato, Desc desc);
}
