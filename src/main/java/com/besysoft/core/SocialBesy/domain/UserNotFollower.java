package com.besysoft.core.SocialBesy.domain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserNotFollower{
    private Long userId;
    private String nombre_usuario;
    private Date date;
}
