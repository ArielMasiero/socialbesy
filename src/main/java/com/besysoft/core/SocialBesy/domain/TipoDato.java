package com.besysoft.core.SocialBesy.domain;

import lombok.Getter;

@Getter
public enum TipoDato {
    NOMBRE("nombre_usuario"),FECHA("date");
    private String descripcion;

    TipoDato(String descripcion) {
        this.descripcion = descripcion;
    }
}
