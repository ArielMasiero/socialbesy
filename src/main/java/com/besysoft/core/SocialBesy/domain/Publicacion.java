package com.besysoft.core.SocialBesy.domain;



import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.stereotype.Component;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Publicacion {
    private Long publicacion_id;
    private Date fecha;
    private Producto detalle;
    private BigDecimal precio;
    private int categoria;
}
