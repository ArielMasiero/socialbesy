package com.besysoft.core.SocialBesy.domain;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Producto {
    private Long product_ID;
    private String producto_nombre;
    private String tipo;
    private String marca;
    private String color;
    private String observaciones;
}
