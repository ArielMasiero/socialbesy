package com.besysoft.core.SocialBesy.domain;

import com.besysoft.core.SocialBesy.dto.UserNotFollowerDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class User {
    private Long UserId;
    private String nombre_usuario;
    private List<UserNotFollower> seguidores;
    private List<UserNotFollower> seguidos;
    private List<Publicacion> publicaciones;

    public User(Long id,String nombre_usuario){
        this.UserId=id;
        this.nombre_usuario = nombre_usuario;
    }
}
