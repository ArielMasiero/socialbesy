package com.besysoft.core.SocialBesy.domain;

import lombok.Getter;

@Getter
public enum Desc {
    SI("SI"),NO("NO");
    private String descripcion;

    Desc(String descripcion) {
        this.descripcion = descripcion;
    }
}
