package com.besysoft.core.SocialBesy.repository;

import com.besysoft.core.SocialBesy.domain.Desc;
import com.besysoft.core.SocialBesy.domain.Publicacion;
import com.besysoft.core.SocialBesy.domain.TipoDato;
import com.besysoft.core.SocialBesy.domain.User;

import java.util.List;
import java.util.Optional;

public interface RepositoryInterface {
    List<User> getAllUsers();
    List<Publicacion> getAllPublicaciones();
    User saveUser(User user);
    Publicacion savePublicacion(Publicacion publicacion);
    Optional<User> getUserByid(Long id);
    Optional<User> getUserFollowerByidOrdernado(Long id, TipoDato tipoDato, Desc desc);
    Optional<User> getUserFollowByidOrdernado(Long id, TipoDato tipoDato, Desc desc);
}
