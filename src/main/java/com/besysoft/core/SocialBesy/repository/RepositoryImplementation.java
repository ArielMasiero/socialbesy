package com.besysoft.core.SocialBesy.repository;

import com.besysoft.core.SocialBesy.domain.*;
import com.besysoft.core.SocialBesy.service.implementation.UserImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;

@Component
public class RepositoryImplementation implements RepositoryInterface{
    Logger log = LoggerFactory.getLogger(RepositoryImplementation.class);
    private List<User> users;
    private List<Publicacion> publicaciones;

    @Autowired
    public RepositoryImplementation(List<User> users, List<Publicacion> publicaciones) {

        publicaciones = Arrays.asList(new Publicacion(1L,new Date(),new Producto(1L,"Mouse","Gamer","logitech","Negro",""),new BigDecimal(1200),100),
                new Publicacion(2L,new Date(),new Producto(2L,"Teclado","Mecanico","Razer","Negro",""),new BigDecimal(14200),100),
                new Publicacion(3L,new Date(),new Producto(3L,"Monitor","Gamer","Gigabyte","Negro","144 FPS"),new BigDecimal(57000),100));

        users= Arrays.asList(
                new User(1L,"Macrino",Arrays.asList(new UserNotFollower(2L,"Loco Lope",new Date())),Arrays.asList(new UserNotFollower(2L,"Loco Lope",new Date())),Arrays.asList(publicaciones.get(0))),
                new User(2L,"Loco Lope",Arrays.asList(new UserNotFollower(1L,"Macrino",new Date())),Arrays.asList(new UserNotFollower(1L,"Macrino",new Date())),Arrays.asList(publicaciones.get(1),publicaciones.get(2))),
                new User(3L,"Ariel Masiero"),
                new User(4L,"Perello")
        );
        this.users = users;
        this.publicaciones = publicaciones;
    }

    @Override
    public List<User> getAllUsers() {
        return users;
    }

    @Override
    public List<Publicacion> getAllPublicaciones() {
        return publicaciones;
    }

    @Override
    public User saveUser(User user) {
        if(user.getUserId()!=null&&existeUsuario(user.getUserId())){
            log.info("usuario no nulo");
            try {
                users.forEach(user1 -> {
                    if (user1.getUserId().equals(user.getUserId())) {
                        user1.setPublicaciones(user.getPublicaciones());
                        user1.setSeguidores(user.getSeguidores());
                        user1.setSeguidos(user.getSeguidos());
                    }
                });
            }catch (Exception e){
                log.error("falla la modificacion de datos");
                log.error(e.getMessage());
            }
        }else{
            users = new ArrayList<>(users);
            user.setUserId(users.stream().count()+1);
            users.add(user);
        }
        return user;
    }

    @Override
    public Publicacion savePublicacion(Publicacion publicacion) {
        publicaciones = new ArrayList<>(publicaciones);
        publicacion.setPublicacion_id(publicaciones.stream().count()+1);
        publicaciones.add(publicacion);
        return publicacion;
    }

    @Override
    public Optional<User> getUserByid(Long id) {
        return users.stream().filter(user -> user.getUserId().equals(id)).findFirst();
    }

    @Override
    public Optional<User> getUserFollowerByidOrdernado(Long id, TipoDato tipoDato, Desc desc) {
        Comparator<UserNotFollower> comparatordate= new Comparator<UserNotFollower>(){
            @Override
            public int compare(UserNotFollower o1, UserNotFollower o2) {
                return o1.getDate().compareTo(o2.getDate());
            }
        };
        Comparator<UserNotFollower> comparatornombre= new Comparator<UserNotFollower>(){
            @Override
            public int compare(UserNotFollower o1, UserNotFollower o2) {
                return o1.getNombre_usuario().compareTo(o2.getNombre_usuario());
            }
        };
        Optional<User> user = this.getUserByid(id);
        if(user.isPresent()){
            List<UserNotFollower> userFollowers = getList(user.get().getSeguidores());
            if(tipoDato.equals(TipoDato.FECHA)){
                if(desc.equals(Desc.NO))userFollowers.sort( comparatordate);
                if(desc.equals(Desc.SI))userFollowers.sort( comparatordate.reversed());
                log.info("Lista seguidores {}",userFollowers);
                user.get().setSeguidores(userFollowers);
            }else{
                if(desc.equals(Desc.NO))userFollowers.sort(comparatornombre);
                if(desc.equals(Desc.SI))userFollowers.sort(comparatornombre.reversed());
                log.info("Lista seguidores {}",userFollowers);
                user.get().setSeguidores(userFollowers);
            }
            return user;
        }else{
            return Optional.empty();
        }
    }

    @Override
    public Optional<User> getUserFollowByidOrdernado(Long id, TipoDato tipoDato, Desc desc) {
        Optional<User> user = this.getUserByid(id);
        Comparator<UserNotFollower> comparatordate= new Comparator<UserNotFollower>(){
            @Override
            public int compare(UserNotFollower o1, UserNotFollower o2) {
                return o1.getDate().compareTo(o2.getDate());
            }
        };
        Comparator<UserNotFollower> comparatornombre= new Comparator<UserNotFollower>(){
            @Override
            public int compare(UserNotFollower o1, UserNotFollower o2) {
                return o1.getNombre_usuario().compareTo(o2.getNombre_usuario());
            }
        };
        if(user.isPresent()){
            List<UserNotFollower> userFollows = getList(user.get().getSeguidos());
            if(tipoDato.equals(TipoDato.FECHA)){
                if(desc.equals(Desc.NO))userFollows.sort( comparatordate);
                if(desc.equals(Desc.SI))userFollows.sort( comparatordate.reversed());
                log.info("Lista empleados {}",userFollows);
                user.get().setSeguidos(userFollows);
            }else{
                if(desc.equals(Desc.NO))userFollows.sort(comparatornombre);
                if(desc.equals(Desc.SI))userFollows.sort(comparatornombre.reversed());
                log.info("Lista empleados {}",userFollows);
                user.get().setSeguidos(userFollows);
            }
            return user;
        }else{
            return Optional.empty();
        }
    }
    private List<UserNotFollower> getList(List<UserNotFollower> list){
        if(list==null||list.size()==0){
            return new ArrayList<>();
        }else{
            return new ArrayList<>(list);
        }
    }
    private Boolean existeUsuario(Long id){
        if(users.stream().anyMatch(user -> user.getUserId().equals(id))){
            return true;
        }else{
            return false;
        }
    }
}