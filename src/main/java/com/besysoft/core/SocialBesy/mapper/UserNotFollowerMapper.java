package com.besysoft.core.SocialBesy.mapper;

import com.besysoft.core.SocialBesy.domain.User;
import com.besysoft.core.SocialBesy.domain.UserNotFollower;
import com.besysoft.core.SocialBesy.dto.UserNotFollowerDto;
import org.mapstruct.Mapper;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface UserNotFollowerMapper {
    @Named("toEntity")
    UserNotFollower getUserNotFollower(UserNotFollowerDto userNotFollowerDto);
    @Named("toDto")
    UserNotFollowerDto getUserNotFollowerDto(UserNotFollower userNotFollower);
    @Named("toDto")
    UserNotFollower getUserNotFollowerToUser(User user);
}
