package com.besysoft.core.SocialBesy.mapper;

import com.besysoft.core.SocialBesy.domain.Producto;
import com.besysoft.core.SocialBesy.dto.ProductoDto;
import org.mapstruct.Mapper;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface ProductoMapper {
    @Named("toDto")
    ProductoDto getProductoDto(Producto producto);
    @Named("toEntity")
    Producto getProducto(ProductoDto productoDto);
}
