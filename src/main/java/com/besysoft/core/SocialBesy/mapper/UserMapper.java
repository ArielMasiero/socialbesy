package com.besysoft.core.SocialBesy.mapper;

import com.besysoft.core.SocialBesy.domain.User;
import com.besysoft.core.SocialBesy.dto.*;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Named;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {
    @Named("toDto")
    UserDto getUserDto(User user);
    @Named("toDto")
    UserOnlyFollowDto getUserOnlyFollowDto(User user);
    @Named("toDto")
    UserOnlyFollowerDto getUserOnlyFollowerDto(User user);
    @Named("toDto")
    UserPublicationDto getUserPublicationDto(User user);
    @Named("toEntity")
    User getUser(UserDto userDto);
    @IterableMapping(qualifiedByName = "toDto")
    List<UserDto> getAllUserDto(List<User> users);
}
