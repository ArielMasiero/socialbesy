package com.besysoft.core.SocialBesy.mapper;

import com.besysoft.core.SocialBesy.domain.Publicacion;
import com.besysoft.core.SocialBesy.domain.User;
import com.besysoft.core.SocialBesy.dto.PublicacionDto;
import com.besysoft.core.SocialBesy.dto.UserDto;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Named;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PublicacionMapper {
    @Named("toDto")
    PublicacionDto getPublicacionDto(Publicacion publicacion);
    @Named("toEntity")
    Publicacion getPublicacion(PublicacionDto publicacionDto);
    @IterableMapping(qualifiedByName = "toDto")
    List<PublicacionDto> getAllPublicacionDto(List<Publicacion> publicacion);
}
