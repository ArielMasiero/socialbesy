package com.besysoft.core.SocialBesy.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class CustomException {

    public CustomException() {
    }

    public static class SocialBesyExceptions extends Exception{
        public SocialBesyExceptions(Throwable throwable){super(throwable);}
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public static class ClienteNoExiste extends SocialBesyExceptions{
        private final Long id;

        public ClienteNoExiste(Long id) {
            super(null);
            this.id = id;
        }

        @Override
        public String getMessage() {
            return "El cliente con id: " + this.id + " no existe.";
        }
    }
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public static class Autoseguirse extends SocialBesyExceptions{
        private final Long id;

        public Autoseguirse(Long id) {
            super(null);
            this.id = id;
        }

        @Override
        public String getMessage() {
            return "El cliente con id: " + this.id + " se esta tratando de seguir a si mismo";
        }
    }
}
