package com.besysoft.core.SocialBesy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserNotFollowerDto {
    private Long userId;
    private String nombre_usuario;
    private Date date;
}
