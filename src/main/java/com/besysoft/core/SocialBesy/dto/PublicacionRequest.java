package com.besysoft.core.SocialBesy.dto;

import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PublicacionRequest {
    private Long UserId;
    private String fecha;
    private ProductoDto detalle;
    private BigDecimal precio;
    private int categoria;
}
