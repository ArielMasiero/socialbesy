package com.besysoft.core.SocialBesy.dto;

import com.besysoft.core.SocialBesy.domain.Publicacion;
import com.besysoft.core.SocialBesy.domain.User;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserDto {
    private Long userId;
    private String nombre_usuario;
    private List<UserNotFollowerDto> seguidores;
    private List<UserNotFollowerDto> seguidos;
    private List<PublicacionDto> publicaciones;
}
