package com.besysoft.core.SocialBesy.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserOnlyFollowerDto {
    private Long userId;
    private String nombre_usuario;
    private List<UserNotFollowerDto> seguidores;
}
