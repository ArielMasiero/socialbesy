package com.besysoft.core.SocialBesy;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SocialBesyApplicationTests {

	@Test
	@Disabled
	void contextLoads() {
	}

}
